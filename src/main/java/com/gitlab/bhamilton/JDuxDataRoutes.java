package com.gitlab.bhamilton;

import com.gitlab.bhamilton.jdux.JDux;
import com.gitlab.bhamilton.jdux.JsonDB;
import com.gitlab.bhamilton.jdux.JsonNode;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.LoggerHandler;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.jboss.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import java.nio.file.Paths;
import java.util.function.BiConsumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@ApplicationScoped
public class JDuxDataRoutes {

    private static final String ROUTE = "/data";
    private static final Pattern PATH_PATTERN = Pattern.compile(ROUTE + "/?(.*)");
    private static final Logger LOG = Logger.getLogger(JDuxDataRoutes.class);

    record ErrorMessage(int statusCode, String type) {}

    private final JsonDB db;

    @Inject
    public JDuxDataRoutes(
        @ConfigProperty(name = "jdux.db.path", defaultValue = "db") String dbFolder
    ) {
        this(JDux.folderDB(Paths.get(dbFolder)));
    }

    public JDuxDataRoutes(JsonDB db) {
        this.db = db;
    }

    public void init(@Observes Router router) {
        router.route(ROUTE + "/*")
            .handler(BodyHandler.create())
            .handler(LoggerHandler.create())
            .handler(this::handleRequest);
    }

    private void handleRequest(RoutingContext rc) {
        try {
            switch (rc.request().method()) {
                case GET -> rc.response()
                    .putHeader("Content-Type", "application/json")
                    .end(JDux.toString(db.select(getPath(rc))));
                case PUT, PATCH -> modify(rc, db::update);
                case POST -> modify(rc, db::insert);
                default -> throw new UnsupportedOperationException();
            }
        } catch (RuntimeException e) {
            handleError(rc, e);
        }
    }

    private static String getPath(RoutingContext rc) {
        Matcher matcher = PATH_PATTERN.matcher(rc.normalisedPath());
        if (!matcher.matches())
            throw new IllegalArgumentException("Invalid path " + rc.normalisedPath());
        return matcher.group(1);
    }

    private static void modify(RoutingContext rc, BiConsumer<String, JsonNode> method) {
        String body = rc.getBodyAsString();
        if (body == null)
            throw new IllegalArgumentException("Body is empty");
        method.accept(getPath(rc), JDux.parse(body));
        rc.response().setStatusCode(201).end();
    }

    private static void handleError(RoutingContext rc, RuntimeException e) {
        ErrorMessage error;
        if (e instanceof IllegalArgumentException)
            error = new ErrorMessage(400, "Invalid Request");
        else if (e instanceof UnsupportedOperationException)
            error = new ErrorMessage(405, "Unsupported Operation");
        else
            error = new ErrorMessage(500, "Internal Error");

        LOG.error("Failed to satisfy request", e);
        rc.response()
            .setStatusCode(error.statusCode())
            .end(error.type() + (e.getMessage() == null ? "" : ": " + e.getMessage()));
    }

}
