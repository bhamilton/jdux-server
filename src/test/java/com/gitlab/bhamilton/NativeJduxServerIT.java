package com.gitlab.bhamilton;

import io.quarkus.test.junit.NativeImageTest;

@NativeImageTest
public class NativeJduxServerIT extends JDuxDataRoutesTest {

    // Execute the same tests but in native mode.
}
